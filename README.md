# guia3u1
Ejercicio1 corresponde al ordenamiento de nùmeros y a ala divisiòn en dos listas, una en la que se encuentran los negativos y otra en la que se encuentran los positivos incluido el 0.
#
Lineas de compilacion
g++ Programa.cpp Lista.cpp -o Programa -o Lista
o make
Linea de ejecucion
./programa
#
Ejercicio2 se crea una lista de nombres/palabras, se pretendia ordenar alfabeticamente, mediante la libreria cstring y el metodo ordenar. La libreria antes mencionadada incluye la posibilidad de usar strcmp para el orden de las palabras.
#
Lineas de compilacion
g++ Programa.cpp Lista.cpp -o Programa -o Lista
o make
Linea de ejecucion
./programa
#
Ejercicio03 se crea una lista de postres en la que se pueden agregar postres y a los postres se pueden agregar ingredientes.
#
Lineas de compilacion
g++ Programa.cpp Lista.cpp -o Programa -o Lista
o make
Linea de ejecucion
./programa
