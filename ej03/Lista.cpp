#include <iostream>
#include <string>
#include <cstring>
using namespace std;
#include "Lista.h"


Lista::Lista() {}

void Lista::crear (string nombres) {
	Nodo *tmp;

	/* Se crea el nuevo nodo */
	tmp = new Nodo;

	/* se asigna una instancia al numero */
	tmp->nombres = nombres;
    
	/* apunta a NULL por defecto.  */
	tmp->sig = NULL;

	/* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
	if (this->raiz == NULL) {
		this->raiz = tmp;

	} else {
		//variables auxiliares para buscar posicion
		Nodo *aux_init, *aux_;
		
		aux_init = this->raiz; 
		
		//para recorrer la lista
		while((aux_init != NULL) && (aux_init->nombres < nombres)){
			aux_ = aux_init;
			aux_init = aux_init->sig;
		}
		
		// se revisa si debe ir al principio de la lista
		if(aux_init == this->raiz){
			this->raiz = tmp;
			this->raiz->sig = tmp;
		}else{
			aux_->sig = tmp;
			tmp->sig = aux_init;
		}

	}
}

void Lista::imprimir () {
	/* variable temporal para recorrer la lista */
	Nodo *tmp = this->raiz;

	/* la recorre mientras sea distinto de NULL */
	while (tmp != NULL) {
		cout << tmp->nombres << " ";
		tmp = tmp->sig;
	}
}


void Lista ::eliminar () {

}

void Lista ::menu_ingredientes () {
	int opcion = 0;
	string ingreso;

	Lista *lista = new Lista();
	
do{
		cout << "\n1.- Agregar ingrediente: " << endl;
		cout << "2.- Mostrar listado de postres" << endl;
		cout << "3.- Eliminar postre" << endl;
		cout << "4.- Salir del menu ingredientes" << endl;
		cout << "Ingrese opcion" << endl;
		cin >> opcion;
		
		switch (opcion){
			case 1: cout << "Ingrese nombre del ingrediente: " << endl;
					cin >> ingreso;
					lista->crear((ingreso));//(stoi(ingreso))
					break;
			case 2: lista->imprimir();
					break;
			case 3: lista->eliminar();//falta crear
			}
		
		}while(opcion !=3);

}
